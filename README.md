# Echo Code Challenge: Echo Data Inspector

This app allows to inspect Patient and Medication data on the Firebase Realtime Database, functionality including editing Patient details, add medication to a Patient, saving Patient data locally in SQLite.

Submitted by: Zohaib Hussain


## Features

The following **Features** are implemented:

* [x] User can **successfully add and edit Patients**
* [x] User can **tap on medication prescribed for a Patient and add medication to a Patient** which reflects changes in Firebase after saving.
* [x] Persist the Patient data **into SQLite**.
* [x] User can **view Medication** data in a list.
* [x] User can **shake device** on the Patient details screen to change background color to white.
* [x] All list use **Recycler view using ViewHolder-Adapter** pattern





## Video Walkthrough

Here's a walkthrough of the implementation user stories:

[Video Walkthrough](http://imgur.com/a/fdCOM)

GIF created with [LiceCap](http://www.cockos.com/licecap/).

## Improvements (things I should have done but ran out of time!):
* There is some repeated code in the Activities, which should be all be implemented into a BaseActivity.
* Only Happy Path has been implemented.
* Implementing a more robust way of tracking Patient and Medication ids.
* There is quite a bit of casting which again could have been improved with implemented a BaseActivity.
* Implement adding and editing Medication data.
* Tested mainly against Happy Path, haven't covered other scenarios.
* SQLite only stores patient data, fetching when Firebase fails should be implemented.
* Rotation has been disabled







## License

    Copyright 2016 Zohaib Hussain

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.