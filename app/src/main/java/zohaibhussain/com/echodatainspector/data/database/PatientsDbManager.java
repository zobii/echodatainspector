package zohaibhussain.com.echodatainspector.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import zohaibhussain.com.echodatainspector.data.models.Patient;

/**
 * Created by zohaibhussain on 2016-10-25.
 */

public class PatientsDbManager {

    private static PatientsDbManager sPatientsDbManager;
    private Context mContext;
    private SQLiteDatabase mDb;

    public static PatientsDbManager get(Context context){
        if (sPatientsDbManager != null)
            return sPatientsDbManager;
        sPatientsDbManager = new PatientsDbManager(context);
        return sPatientsDbManager;
    }

    private PatientsDbManager(Context context) {
        mContext = context.getApplicationContext();
        mDb = new PatientBaseHelper(mContext).getWritableDatabase();
    }


    public void addPatient(Patient patient){
        mDb.insert(PatientDbSchema.PatientTable.Name, null, getContentValues(patient));
    }

    public void addPatients(List<Patient> patientList){
        mDb.delete(PatientDbSchema.PatientTable.Name, null, null);
        for (Patient patient: patientList){
            mDb.insert(PatientDbSchema.PatientTable.Name, null, getContentValues(patient));
        }
    }


    private ContentValues getContentValues(Patient patient) {
        ContentValues cv = new ContentValues();
        cv.put(PatientDbSchema.Cols.ID, patient.getId());
        cv.put(PatientDbSchema.Cols.NAME, patient.getName());
        cv.put(PatientDbSchema.Cols.SURNAME, patient.getSurname());
        cv.put(PatientDbSchema.Cols.BIRTHDATE, patient.getBirthdate());
        cv.put(PatientDbSchema.Cols.EMAIL, patient.getEmail());
        return cv;
    }

}


