package zohaibhussain.com.echodatainspector.data.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zohaibhussain on 2016-10-21.
 */

public class Patient implements Serializable {

    private String id;
    private String name;
    private String surname;
    private String email;
    private String birthdate;
    private List<String> medicine_prescribed;

    public Patient() {
    }

    public Patient(String id, String name, String surname, String email, String birthdate, List<String> medicineIdPrescribed) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.birthdate = birthdate;
        this.medicine_prescribed = medicineIdPrescribed;
    }

    public Patient(String name, String surname, String email, String birthdate, List<String> medicine_prescribed) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.birthdate = birthdate;
        this.medicine_prescribed = medicine_prescribed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public List<String> getMedicine_prescribed() {
        return medicine_prescribed;
    }

    public void setMedicine_prescribed(List<String> medicine_prescribed) {
        this.medicine_prescribed = medicine_prescribed;
    }
}
