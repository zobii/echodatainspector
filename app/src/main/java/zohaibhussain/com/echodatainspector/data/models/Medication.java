package zohaibhussain.com.echodatainspector.data.models;

/**
 * Created by zohaibhussain on 2016-10-21.
 */

public class Medication {
    private String id;
    private String name;
    private String category;
    private String detail;

    public Medication() {
    }

    public Medication(String id, String name, String category, String detail) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.detail = detail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
