package zohaibhussain.com.echodatainspector.data;

import java.util.ArrayList;
import java.util.List;

import zohaibhussain.com.echodatainspector.data.models.Patient;

/**
 * Created by zohaibhussain on 2016-10-21.
 */

public class PatientDataSingleton {
    private List<Patient> patientList;
    private static PatientDataSingleton patientDataSingletonInstance;

    public static PatientDataSingleton getInstance(){
        if (patientDataSingletonInstance == null){
            patientDataSingletonInstance = new PatientDataSingleton();
        }
        return patientDataSingletonInstance;
    }

    private PatientDataSingleton() {
        patientList = new ArrayList<>();
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }
}
