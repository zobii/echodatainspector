package zohaibhussain.com.echodatainspector.data;

import java.util.ArrayList;
import java.util.List;

import zohaibhussain.com.echodatainspector.data.models.Medication;
import zohaibhussain.com.echodatainspector.data.models.Patient;

/**
 * Created by zohaibhussain on 2016-10-24.
 */

public class MedicationDataSingleton {
    private static MedicationDataSingleton medicationDataSingletonInstance;
    private List<Medication> medicationList = new ArrayList<>();

    public static MedicationDataSingleton getInstance(){
        if (medicationDataSingletonInstance == null) {
            medicationDataSingletonInstance = new MedicationDataSingleton();
        }
        return medicationDataSingletonInstance;
    }

    private MedicationDataSingleton() {
    }

    private MedicationDataSingleton(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public List<Medication> getPatientSpecificMedication(Patient patient){
        List<Medication> patientMedication = new ArrayList<>();
        if (patient.getMedicine_prescribed() == null || patient.getMedicine_prescribed().size()==0)
            return patientMedication;
        for (String medPrescribedId : patient.getMedicine_prescribed()){
            for (Medication medication : medicationList){
                if (medPrescribedId.equals(medication.getId()))
                    patientMedication.add(medication);
            }
        }
        return patientMedication;
    }
}
