package zohaibhussain.com.echodatainspector.data.database;

/**
 * Created by zohaibhussain on 2016-10-25.
 */

public class PatientDbSchema {
    public static final class PatientTable{
        public static final String Name = "PATIENTS";
    }
    public static final class Cols{
        public static final String ID = "uuid";
        public static final String NAME = "name";
        public static final String SURNAME = "surname";
        public static final String BIRTHDATE = "birthdate";
        public static final String EMAIL = "email";
    }
}