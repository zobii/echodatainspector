package zohaibhussain.com.echodatainspector.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by zohaibhussain on 2016-10-25.
 */

public class PatientBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME= "patients.db";

    public PatientBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + PatientDbSchema.PatientTable.Name + "("
                + " _id integer primary key autoincrement, "
                + PatientDbSchema.Cols.ID + ", "
                + PatientDbSchema.Cols.NAME + ", "
                + PatientDbSchema.Cols.SURNAME + ", "
                + PatientDbSchema.Cols.BIRTHDATE + ", "
                + PatientDbSchema.Cols.EMAIL + ")"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
