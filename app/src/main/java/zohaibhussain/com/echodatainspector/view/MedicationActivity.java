package zohaibhussain.com.echodatainspector.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import zohaibhussain.com.echodatainspector.data.models.Medication;
import zohaibhussain.com.echodatainspector.data.MedicationDataSingleton;
import zohaibhussain.com.echodatainspector.R;
import zohaibhussain.com.echodatainspector.view.fragments.MedicationListFragment;

public class MedicationActivity extends Activity {

    public static final String MEDICATON_NODE = "medication";

    @Bind(R.id.fragment_container)
    protected FrameLayout mFragmentContainer;

    @Bind(R.id.loadingProgressBar)
    protected ProgressBar mLoadingProgressBar;

    private DatabaseReference mMedicationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);
        ButterKnife.bind(this);
        mMedicationData = FirebaseDatabase.getInstance().getReference().child(MEDICATON_NODE);
        showLoading();
        mMedicationData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideLoading();
                List<Medication> medicationList = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    medicationList.add(child.getValue(Medication.class));
                }
                MedicationDataSingleton.getInstance().setMedicationList(medicationList);
                if (getFragmentManager().findFragmentByTag(MedicationListFragment.class.getSimpleName()) != null) {
                    MedicationListFragment fragment = (MedicationListFragment) getFragmentManager().findFragmentByTag(MedicationListFragment.class.getSimpleName());
                    fragment.newDataLoaded(MedicationDataSingleton.getInstance().getMedicationList());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        replaceFragment(MedicationListFragment.getNewInstance());
    }

    protected void showLoading(){
        mLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading(){
        mLoadingProgressBar.setVisibility(View.GONE);
    }

    public void replaceFragment(Fragment fragment) {
        if (this != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
