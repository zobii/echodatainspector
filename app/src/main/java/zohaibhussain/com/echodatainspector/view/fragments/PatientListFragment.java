package zohaibhussain.com.echodatainspector.view.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zohaibhussain.com.echodatainspector.data.models.Patient;
import zohaibhussain.com.echodatainspector.data.PatientDataSingleton;
import zohaibhussain.com.echodatainspector.R;
import zohaibhussain.com.echodatainspector.view.PatientsActivity;


public class PatientListFragment extends Fragment {

    private static final String PATIENT_LIST = "PATIENT_LIST";

    @Bind(R.id.patient_recyclerview)
    RecyclerView mPatientRecyclerView;

    List<Patient> mPatients;

    public static PatientListFragment getNewInstance(List<Patient> patientList){
        PatientListFragment fragment = new PatientListFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT_LIST, (Serializable) patientList);
        fragment.setArguments(args);
        return fragment;
    }

    public PatientListFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_list, container, false);
        ButterKnife.bind(this, v);
        getActivity().setTitle("Patient List");
        mPatients = PatientDataSingleton.getInstance().getPatientList();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPatientRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPatientRecyclerView.setAdapter(new PatientAdapter(mPatients));
    }

    public void newDataLoaded(List<Patient> patientList) {
        mPatients = patientList;
        ((PatientAdapter)mPatientRecyclerView.getAdapter()).setPatientList(mPatients);
        mPatientRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @OnClick(R.id.add_patient_fab)
    protected void onAddPatientFabClicked(){
        ((PatientsActivity)getActivity()).replaceFragment(zohaibhussain.com.echodatainspector.view.fragments.PatientInfoFragment.getNewInstance());
    }

    protected class PatientAdapter extends RecyclerView.Adapter<PatientViewHolder>{

        protected List<Patient> patientList;

        public PatientAdapter(List<Patient> patientList) {
            super();
            this.patientList = patientList;
        }

        @Override
        public PatientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.patient_row, parent, false);
            return new PatientViewHolder(v);
        }

        @Override
        public void onBindViewHolder(PatientViewHolder holder, int position) {
            holder.bind(patientList.get(position));
        }

        @Override
        public int getItemCount() {
            return patientList.size();
        }

        public void setPatientList(List<Patient> patientList) {
            this.patientList = patientList;
        }
    }

    protected class PatientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Patient patient;

        private TextView patientNameTextview;
        private TextView patientEmailTextview;
        private TextView patientBirthdateTextview;

        public PatientViewHolder(View itemView) {
            super(itemView);
            patientNameTextview = (TextView) itemView.findViewById(R.id.medication_name_textview);
            patientEmailTextview = (TextView) itemView.findViewById(R.id.medication_detail_textview);;
            patientBirthdateTextview = (TextView) itemView.findViewById(R.id.medication_category_textview);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            ((PatientsActivity)getActivity()).replaceFragment(zohaibhussain.com.echodatainspector.view.fragments.PatientInfoFragment.getNewInstance(patient));
        }

        public void bind(Patient patient) {
            this.patient = patient;
            patientNameTextview.setText(patient.getName()+" "+patient.getSurname());
            patientEmailTextview.setText(patient.getEmail());
            patientBirthdateTextview.setText(patient.getBirthdate());
        }
    }
}


