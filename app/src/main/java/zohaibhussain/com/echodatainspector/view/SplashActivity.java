package zohaibhussain.com.echodatainspector.view;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zohaibhussain.com.echodatainspector.R;

public class SplashActivity extends Activity {

    @Bind(R.id.videoView)
    protected VideoView mVideoView;

    private MediaPlayer mMediaPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
    }

    private void initVideo() {
        mVideoView.setVideoPath("android.resource://" + getPackageName() + "/"+ R.raw.vertical_echo_intro);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mMediaPlayer = mediaPlayer;
                mediaPlayer.setLooping(true);
            }
        });
        mVideoView.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoView != null && mMediaPlayer != null) {
            mVideoView.stopPlayback();
            mMediaPlayer.release();
        }
    }

    @OnClick(R.id.start_button)
    protected void onStartClick(){
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }
}