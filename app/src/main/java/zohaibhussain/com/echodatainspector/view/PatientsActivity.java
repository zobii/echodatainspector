package zohaibhussain.com.echodatainspector.view;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import zohaibhussain.com.echodatainspector.data.database.PatientsDbManager;
import zohaibhussain.com.echodatainspector.data.models.Medication;
import zohaibhussain.com.echodatainspector.data.MedicationDataSingleton;
import zohaibhussain.com.echodatainspector.data.models.Patient;
import zohaibhussain.com.echodatainspector.data.PatientDataSingleton;
import zohaibhussain.com.echodatainspector.R;
import zohaibhussain.com.echodatainspector.view.fragments.PatientInfoFragment;
import zohaibhussain.com.echodatainspector.view.fragments.PatientListFragment;

public class PatientsActivity extends Activity {

    public static final String MEDICATON_NODE = "medication";
    public static final String PATIENT_NODE = "patient";

    @Bind(R.id.fragment_container)
    protected FrameLayout mFragmentContainer;

    @Bind(R.id.loadingProgressBar)
    protected ProgressBar mLoadingProgressBar;

    private DatabaseReference mPatientsData;
    private DatabaseReference mMedicationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);
        ButterKnife.bind(this);
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        mPatientsData = database.child(PATIENT_NODE);
        showLoading();
        mPatientsData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideLoading();
                List<Patient> patientList = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    patientList.add(child.getValue(Patient.class));
                }
                PatientDataSingleton.getInstance().setPatientList(patientList);
                PatientListFragment fragment = (PatientListFragment) findFragment(PatientListFragment.class);
                if (fragment != null)
                    fragment.newDataLoaded(PatientDataSingleton.getInstance().getPatientList());
                savePatientListToDatabase();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mMedicationData = FirebaseDatabase.getInstance().getReference().child(MEDICATON_NODE);
        mMedicationData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Medication> medicationList = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    medicationList.add(child.getValue(Medication.class));
                }
                MedicationDataSingleton.getInstance().setMedicationList(medicationList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        replaceFragment(PatientListFragment.getNewInstance(PatientDataSingleton.getInstance().getPatientList()));
    }

    private void savePatientListToDatabase() {
        PatientsDbManager.get(getApplicationContext()).addPatients(PatientDataSingleton.getInstance().getPatientList());
    }

    protected Fragment findFragment(Class fragmentClass) {
        if (getFragmentManager().findFragmentByTag(fragmentClass.getSimpleName()) != null) {
            return getFragmentManager().findFragmentByTag(fragmentClass.getSimpleName());
        }
        return null;
    }

    public void replaceFragment(Fragment fragment) {
        if (this != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            finish();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void saveNewPatient(Patient mPatient) {
        String newId = String.valueOf(PatientDataSingleton.getInstance().getPatientList().size() + 1);
        mPatient.setId(newId);
        mPatientsData.child(String.valueOf(PatientDataSingleton.getInstance().getPatientList().size())).setValue(mPatient);
        popCurrentFragment();
        Toast.makeText(this, "Changes Saved!",Toast.LENGTH_SHORT).show();
    }

    private void popCurrentFragment() {
        getFragmentManager().popBackStack();
    }

    public void savePatient(Patient patient) {
        mPatientsData.child(Integer.valueOf(patient.getId()).intValue() - 1 +"").setValue(patient);
        popCurrentFragment();
        Toast.makeText(this, "Changes Saved!",Toast.LENGTH_SHORT).show();
    }

    public void addMedicationToPatient(Patient patient, Medication medication) {
        PatientInfoFragment fragment = (PatientInfoFragment) findFragment(PatientInfoFragment.class);
        if (fragment != null)
            fragment.medicationAdded(medication);
        popCurrentFragment();
        Toast.makeText(this, "Medication Added to Patient!",Toast.LENGTH_SHORT).show();

    }

    protected void showLoading(){
        mLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading(){
        mLoadingProgressBar.setVisibility(View.GONE);
    }

}
