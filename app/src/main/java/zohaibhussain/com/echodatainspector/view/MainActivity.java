package zohaibhussain.com.echodatainspector.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import zohaibhussain.com.echodatainspector.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.patients_cardView)
    protected void onPatientsClicked(){
        startActivity(new Intent(MainActivity.this, PatientsActivity.class));
    }

    @OnClick(R.id.medication_cardView)
    protected void onMedicationClicked(){
        startActivity(new Intent(MainActivity.this, MedicationActivity.class));
    }
}
