package zohaibhussain.com.echodatainspector.view.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import zohaibhussain.com.echodatainspector.data.models.Medication;
import zohaibhussain.com.echodatainspector.data.MedicationDataSingleton;
import zohaibhussain.com.echodatainspector.data.models.Patient;
import zohaibhussain.com.echodatainspector.R;
import zohaibhussain.com.echodatainspector.view.PatientsActivity;

public class MedicationListFragment extends Fragment {

    private static final String PATIENT_KEY = "PATIENT";

    @Bind(R.id.medication_recycler)
    RecyclerView mMedicationRecyclerView;

    List<Medication> mMedicationList;

    Patient mPatient;

    public static MedicationListFragment getNewInstance(){
        return new MedicationListFragment();
    }

    public static MedicationListFragment getNewInstance(Patient mPatient){
        MedicationListFragment fragment = new MedicationListFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT_KEY, mPatient);
        fragment.setArguments(args);
        return fragment;
    }

    public MedicationListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().getSerializable(PATIENT_KEY) != null)
            mPatient = (Patient) getArguments().getSerializable(PATIENT_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_medication_list, container, false);
        ButterKnife.bind(this, v);
        if (mPatient == null)
            getActivity().setTitle("Medication List");
        else
            getActivity().setTitle("Select a Medication");

        mMedicationList = MedicationDataSingleton.getInstance().getMedicationList();
        mMedicationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMedicationRecyclerView.setAdapter(new MedicationAdapter());
        return v;
    }

    public void newDataLoaded(List<Medication> medicationList){
        mMedicationList = medicationList;
        mMedicationRecyclerView.getAdapter().notifyDataSetChanged();
    }

    protected class MedicationAdapter extends RecyclerView.Adapter<MedicationViewHolder>{

        @Override
        public MedicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.medication_row, parent, false);
            return new MedicationViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MedicationViewHolder holder, int position) {
            holder.bind(mMedicationList.get(position));
        }

        @Override
        public int getItemCount() {
            return mMedicationList.size();
        }
    }


    protected class MedicationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private Medication medication;
        private TextView medicationNameTextview;
        private TextView medicationDetailsTextview;
        private TextView medicationCategoryTextview;


        public MedicationViewHolder(View itemView) {
            super(itemView);
            medicationNameTextview = (TextView) itemView.findViewById(R.id.medication_name_textview);
            medicationDetailsTextview = (TextView) itemView.findViewById(R.id.medication_detail_textview);
            medicationCategoryTextview = (TextView) itemView.findViewById(R.id.medication_category_textview);
            itemView.setOnClickListener(this);
        }

        public void bind(Medication medication) {
            this.medication = medication;
            medicationNameTextview.setText(medication.getName());
            medicationDetailsTextview.setText(medication.getDetail());
            medicationCategoryTextview.setText(medication.getCategory());
        }

        @Override
        public void onClick(View view) {
            if (mPatient != null){
                ((PatientsActivity)getActivity()).addMedicationToPatient(mPatient, medication);
            }
        }
    }

}
