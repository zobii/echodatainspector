package zohaibhussain.com.echodatainspector.view.fragments;


import android.animation.AnimatorInflater;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.seismic.ShakeDetector;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zohaibhussain.com.echodatainspector.data.models.Medication;
import zohaibhussain.com.echodatainspector.data.MedicationDataSingleton;
import zohaibhussain.com.echodatainspector.data.models.Patient;
import zohaibhussain.com.echodatainspector.R;
import zohaibhussain.com.echodatainspector.view.PatientsActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class PatientInfoFragment extends Fragment {
    private static final String PATIENT_KEY = "PATIENT";


    private Patient mPatient;
    private List<Medication> mPatientMedication = new ArrayList<>();
    private ShakeDetector mShakeDetector;

    @Bind(R.id.patient_name)
    protected EditText mNameEditText;

    @Bind(R.id.patient_surname)
    protected EditText mSurNameEditText;


    @Bind(R.id.patient_email)
    protected EditText mEmailEditText;

    @Bind(R.id.patient_birthdate)
    protected EditText mBirthdateEditText;

    @Bind(R.id.medication_recycler)
    protected RecyclerView mMedicationRecyclerView;

    public static PatientInfoFragment getNewInstance(Patient patient){
        PatientInfoFragment fragment = new PatientInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT_KEY, patient);
        fragment.setArguments(args);
        return fragment;
    }

    public static PatientInfoFragment getNewInstance(){
        PatientInfoFragment fragment = new PatientInfoFragment();
        return fragment;
    }

    public PatientInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mPatient = (Patient) getArguments().getSerializable(PATIENT_KEY);
        if (mPatient != null)
            mPatientMedication = MedicationDataSingleton.getInstance().getPatientSpecificMedication(mPatient);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.getSerializable(PATIENT_KEY) != null){
            mPatient = (Patient) savedInstanceState.getSerializable(PATIENT_KEY);

        }
        View v = inflater.inflate(R.layout.fragment_patient_details, container, false);
        getActivity().setTitle("Patient Details");
        ButterKnife.bind(this,v);
/*        if (mPatient != null)
            mPatientMedication = MedicationDataSingleton.getInstance().getPatientSpecificMedication(mPatient);*/
        mMedicationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMedicationRecyclerView.setAdapter(new MedicationAdapter());
        setUpShakeDetector();
        return v;
    }

    private void setUpShakeDetector() {
        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);
        mShakeDetector = new ShakeDetector(new ShakeDetector.Listener() {
            @Override
            public void hearShake() {
                Toast.makeText(getActivity(), "Shake Detected!", Toast.LENGTH_SHORT).show();
                mShakeDetector.stop();
                ObjectAnimator objAnim = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.background_color_changer);
                objAnim.setTarget(getView());
                objAnim.setEvaluator(new ArgbEvaluator());
                objAnim.start();
            }
        });
        mShakeDetector.start(sensorManager);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mPatient != null){
            mNameEditText.setText(mPatient.getName());
            mSurNameEditText.setText(mPatient.getSurname());
            mEmailEditText.setText(mPatient.getEmail());
            mBirthdateEditText.setText(mPatient.getBirthdate());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mShakeDetector.stop();
        mShakeDetector = null;
    }

    @OnClick(R.id.save_button)
    protected void onSaveButtonClicked() {
        if (isValid()) {
            if (mPatient == null) {
                List<String> medicineIDList = getMedicineIdList();
                mPatient = new Patient(mNameEditText.getText().toString(), mNameEditText.getText().toString(), mSurNameEditText.getText().toString(), mEmailEditText.getText().toString(), mBirthdateEditText.getText().toString(), medicineIDList);
                ((PatientsActivity) getActivity()).saveNewPatient(mPatient);
            } else {
                mPatient.setName(mNameEditText.getText().toString());
                mPatient.setSurname(mSurNameEditText.getText().toString());
                mPatient.setEmail(mEmailEditText.getText().toString());
                mPatient.setBirthdate(mBirthdateEditText.getText().toString());
                mPatient.setMedicine_prescribed(getMedicineIdList());
                ((PatientsActivity) getActivity()).savePatient(mPatient);
            }
        }
    }

    @OnClick(R.id.add_medicine)
    protected void onMedicationAddButtonClicked(){
        ((PatientsActivity)getActivity()).replaceFragment(MedicationListFragment.getNewInstance(mPatient));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PATIENT_KEY, mPatient);
    }

    public void medicationAdded(Medication medication) {
        mPatientMedication.add(medication);
        mMedicationRecyclerView.getAdapter().notifyDataSetChanged();
    }

    public List<String> getMedicineIdList() {
        List<String> medicineIdList = new ArrayList<>();
        for (Medication medicine:mPatientMedication){
            medicineIdList.add(medicine.getId());
        }
        return medicineIdList;
    }

    public boolean isValid() {
        if (TextUtils.isEmpty(mNameEditText.getText())||
                TextUtils.isEmpty(mSurNameEditText.getText())||
                TextUtils.isEmpty(mBirthdateEditText.getText())||
                TextUtils.isEmpty(mEmailEditText.getText())){
            Toast.makeText(getActivity(), "Please enter all patient information!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    protected class MedicationAdapter extends RecyclerView.Adapter<MedicationViewHolder>{

        @Override
        public MedicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.medication_row, parent, false);
            return new MedicationViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MedicationViewHolder holder, int position) {
            holder.bind(mPatientMedication.get(position));
        }

        @Override
        public int getItemCount() {
            return mPatientMedication.size();
        }
    }


    protected class MedicationViewHolder extends RecyclerView.ViewHolder{

        private TextView medicationNameTextview;
        private TextView medicationDetailsTextview;
        private TextView medicationCategoryTextview;


        public MedicationViewHolder(View itemView) {
            super(itemView);
            medicationNameTextview = (TextView) itemView.findViewById(R.id.medication_name_textview);
            medicationDetailsTextview = (TextView) itemView.findViewById(R.id.medication_detail_textview);
            medicationCategoryTextview = (TextView) itemView.findViewById(R.id.medication_category_textview);
        }

        public void bind(Medication medication) {
            medicationNameTextview.setText(medication.getName());
            medicationDetailsTextview.setText(medication.getDetail());
            medicationCategoryTextview.setText(medication.getCategory());
        }
    }
}
